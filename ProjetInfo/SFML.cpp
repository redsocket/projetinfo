#include <SFML/Graphics.hpp>
#include <stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>
#define PI 3.1415926
using namespace std;

bool isSpriteHover(sf::FloatRect sprite, sf::Vector2f mp)
{
    if (sprite.contains(mp)) {
        return true;
    }
    return false;
}

int main() {

    //Chargement des polices
    sf::Font font;
    if (!font.loadFromFile("digital-7.ttf"))
    {
        std::cerr << "Erreur ouverture police digital-7.ttf" << std::endl;
    }
    sf::Font font2;
    if (!font2.loadFromFile("arial.ttf"))
    {
        std::cerr << "Erreur ouverture police arial.ttf" << std::endl;
    }

    //Anti-alliasing permettant de "lisser" les formes et d'�viter aux lignes de disparaitre
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    //Cr�ation de la fen�tre
    sf::VideoMode DesktopMode = sf::VideoMode::getDesktopMode(); // Adaptation auto taille fenetre
    sf::RenderWindow window(DesktopMode, "Matematica", sf::Style::Fullscreen, settings); //Fenetre SFML

    //R�cup�ration de la taille de la fen�tre
    sf::Vector2u size = window.getSize();
    int width = size.x;
    int height = size.y;

    //Variables
    int tailledem = 24;
    int taillecase = (int)((width * 0.75) / tailledem);
    int tmpx = width;
    int tmpy = height;
    string s;

    //Centrage de la vue et gestion du zoom
    sf::View view(sf::Vector2f(width * 0.625, height * 0.625), sf::Vector2f(0.75 * width, 0.75 * height));
    view.setViewport(sf::FloatRect(0.25f, 0.25f, 0.75, 0.75f));
    view.setCenter(0, 0);
    view.zoom(2);

    //////////////////////////////////Chargement des sprites///////////////////////
    sf::Texture fleche;
    if (!fleche.loadFromFile("fleche.png"))
    {
        cout << "Erreur lors de la lecture du fichier fleche.png";
    }

    /////////////////////////////////Fl�ches//////////////////////////////////////

    //Fl�che droite
    sf::Sprite flechesprited;
    flechesprited.setTexture(fleche);
    //Fl�che gauche
    sf::Sprite flechespriteg;
    flechespriteg.setTexture(fleche);
    flechespriteg.rotate(180);
    //Fl�che haut
    sf::Sprite flechespriteh;
    flechespriteh.setTexture(fleche);
    flechespriteh.rotate(90);
    //Fl�che bas
    sf::Sprite flechespriteb;
    flechespriteb.setTexture(fleche);
    flechespriteb.rotate(-90);

    //////////////////Boutons calculatrice//////////////////////
    float taillebtn = width * 0.25 * 0.25 * 0.81;
    float radiusbtn = width * 0.25 * 0.25 * 0.81*0.5;
    float ligne1 = height - width * 0.02 * 0.25 - taillebtn*1.07;
    float ligne2 = height - width * 0.04 * 0.25 - taillebtn*2.14;
    float ligne3 = height - width * 0.06 * 0.25 - taillebtn*3.21;
    float ligne4 = height - width * 0.08 * 0.25 - taillebtn*4.28;
    float ligne5 = height - width * 0.1 * 0.25 - taillebtn*5.35;
    float ligne6 = height - width * 0.12 * 0.25 - taillebtn*6.42;
    float ligne7 = height - width * 0.14 * 0.25 - taillebtn*7.49;
    float colonne1 = width * (float)(0.25 * 0.02);
    float colonne2 = width * (float)(0.25 * 0.25 * 0.9) + width * (float)(0.25 * 0.04);
    float colonne3 = width * (float)(0.5 * 0.25 * 0.9) + width * (float)(0.25 * 0.06);
    float colonne4 = width * (float)(0.75 * 0.25 * 0.9) + width * float(0.25 * 0.08);
    float colonne1text = colonne1+radiusbtn*0.75;
    float colonne2text = colonne2 + radiusbtn * 0.75;
    float colonne3text = colonne3 + radiusbtn * 0.75;
    float colonne4text = colonne4 + radiusbtn * 0.75;
    float ligne1text = ligne1 + radiusbtn * 0.35;
    float ligne2text = ligne2 + radiusbtn * 0.35;
    float ligne3text = ligne3 + radiusbtn * 0.35;
    float ligne4text = ligne4 + radiusbtn * 0.35;


    //Bouton 0
    sf::CircleShape bouton0(radiusbtn);
    bouton0.setFillColor(sf::Color(51, 51, 51));
    bouton0.setPosition(colonne1, ligne1);//POS A SET APRES ET CREER CLASSE
    sf::Text textbouton0("0", font2, 50);
    textbouton0.setPosition(colonne1text, ligne1text);
    textbouton0.setFillColor(sf::Color::White);

    //Bouton 1
    sf::CircleShape bouton1(radiusbtn);
    bouton1.setFillColor(sf::Color(51, 51, 51));
    bouton1.setPosition(colonne1, ligne2); // LE -100 = taille bouton
    sf::Text textbouton1("1", font2, 50);
    textbouton1.setPosition(colonne1text, ligne2text);
    textbouton1.setFillColor(sf::Color::White);

    //Bouton 2
    sf::CircleShape bouton2(radiusbtn);
    bouton2.setFillColor(sf::Color(51, 51, 51));
    bouton2.setPosition(colonne2, ligne2);
    sf::Text textbouton2("2", font2, 50);
    textbouton2.setPosition(colonne2text, ligne2text);
    textbouton2.setFillColor(sf::Color::White);

    //Bouton 3
    sf::CircleShape bouton3(radiusbtn);
    bouton3.setFillColor(sf::Color(51, 51, 51));
    bouton3.setPosition(colonne3, ligne2);
    sf::Text textbouton3("3", font2, 50);
    textbouton3.setPosition(colonne3text, ligne2text);
    textbouton3.setFillColor(sf::Color::White);

    //Bouton 4
    sf::CircleShape bouton4(radiusbtn);
    bouton4.setFillColor(sf::Color(51, 51, 51));
    bouton4.setPosition(colonne1, ligne3);
    sf::Text textbouton4("4", font2, 50);
    textbouton4.setPosition(colonne1text,ligne3text);
    textbouton4.setFillColor(sf::Color::White);

    //Bouton 5;
    sf::CircleShape bouton5(radiusbtn);
    bouton5.setFillColor(sf::Color(51, 51, 51));
    bouton5.setPosition(colonne2, ligne3);
    sf::Text textbouton5("5", font2, 50);
    textbouton5.setPosition(colonne2text, ligne3text);
    textbouton5.setFillColor(sf::Color::White);

    //Bouton 6
    sf::CircleShape bouton6(radiusbtn);
    bouton6.setFillColor(sf::Color(51, 51, 51));
    bouton6.setPosition(colonne3, ligne3);
    sf::Text textbouton6("6", font2, 50);
    textbouton6.setPosition(colonne3text, ligne3text);
    textbouton6.setFillColor(sf::Color::White);

    //Bouton 7
    sf::CircleShape bouton7(radiusbtn);
    bouton7.setFillColor(sf::Color(51, 51, 51));
    bouton7.setPosition(colonne1, ligne4);
    sf::Text textbouton7("7", font2, 50);
    textbouton7.setPosition(colonne1text, ligne4text);
    textbouton7.setFillColor(sf::Color::White);

    //Bouton 8
    sf::CircleShape bouton8(radiusbtn);
    bouton8.setFillColor(sf::Color(51, 51, 51));
    bouton8.setPosition(colonne2, ligne4);
    sf::Text textbouton8("8", font2, 50);
    textbouton8.setPosition(colonne2text, ligne4text);
    textbouton8.setFillColor(sf::Color::White);

    //Bouton 9
    sf::CircleShape bouton9(radiusbtn);
    bouton9.setFillColor(sf::Color(51, 51, 51));
    bouton9.setPosition(colonne3, ligne4);
    sf::Text textbouton9("9", font2, 50);
    textbouton9.setPosition(colonne3text, ligne4text);
    textbouton9.setFillColor(sf::Color::White);

    //Bouton point
    sf::CircleShape boutonpoint(radiusbtn);
    boutonpoint.setFillColor(sf::Color(51, 51, 51));
    boutonpoint.setPosition(colonne2, ligne1);
    sf::Text textboutonpoint(".", font2, 50);
    textboutonpoint.setPosition(colonne2text, height - width * 0.02 * 0.25 - 80);
    textboutonpoint.setFillColor(sf::Color::White);

    //Bouton virgule
    sf::CircleShape boutonvirgule(radiusbtn);
    boutonvirgule.setFillColor(sf::Color(51, 51, 51));
    boutonvirgule.setPosition(colonne3, ligne1);
    sf::Text textboutonvirgule(",", font2, 50);
    textboutonvirgule.setPosition(colonne3text, height - width * 0.02 * 0.25 - 80);
    textboutonvirgule.setFillColor(sf::Color::White);

    //Bouton moins
    sf::CircleShape boutonmoins(radiusbtn);
    boutonmoins.setFillColor(sf::Color(230, 134, 0));
    boutonmoins.setPosition(colonne4, ligne1);
    sf::Text textboutonmoins("-", font2, 50);
    textboutonmoins.setPosition(colonne4text*1.015, ligne1text);
    textboutonmoins.setFillColor(sf::Color::White);

    //Bouton plus
    sf::CircleShape boutonplus(radiusbtn);
    boutonplus.setFillColor(sf::Color(230, 134, 0));
    boutonplus.setPosition(colonne4, ligne2);
    sf::Text textboutonplus("+", font2, 50);
    textboutonplus.setPosition(colonne4text, ligne2text);
    textboutonplus.setFillColor(sf::Color::White);

    //Bouton fois
    sf::CircleShape boutonfois(radiusbtn);
    boutonfois.setFillColor(sf::Color(230, 134, 0));
    boutonfois.setPosition(colonne4, ligne3);
    sf::Text textboutonfois("�", font2, 50);
    textboutonfois.setPosition(colonne4text, ligne3text);
    textboutonfois.setFillColor(sf::Color::White);

    //Bouton divis�
    sf::CircleShape boutondivise(radiusbtn);
    boutondivise.setFillColor(sf::Color(230, 134, 0));
    boutondivise.setPosition(colonne4, ligne4);
    sf::Text textboutondivise("�", font2, 50);
    textboutondivise.setPosition(colonne4text, ligne4text);
    textboutondivise.setFillColor(sf::Color::White);
    //Bouton sin
    sf::CircleShape boutonsin(radiusbtn);
    boutonsin.setFillColor(sf::Color(33, 33, 33));
    boutonsin.setPosition(colonne1, ligne5);
    sf::Text textboutonsin("sin", font2, 30);
    textboutonsin.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 - width * 0.02 * 0.125, height - width * 0.1 * 0.25 - 80 - 400);
    textboutonsin.setFillColor(sf::Color::White);

    //Bouton cos
    sf::CircleShape boutoncos(radiusbtn);
    boutoncos.setFillColor(sf::Color(33, 33, 33));
    boutoncos.setPosition(colonne2, ligne5);
    sf::Text textboutoncos("cos", font2, 30);
    textboutoncos.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1 * width * 0.02 * 0.125 + width * 0.25 * 0.25 * 0.9, height - width * 0.02 -480);
    textboutoncos.setFillColor(sf::Color::White);

    //Bouton tan
    sf::CircleShape boutontan(radiusbtn);
    boutontan.setFillColor(sf::Color(33, 33, 33));
    boutontan.setPosition(colonne3, ligne5);
    sf::Text textboutontan("tan", font2, 30);
    textboutontan.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height - width * 0.02 - 480);
    textboutontan.setFillColor(sf::Color::White);
    //Bouton ParentheseG
    sf::CircleShape boutonParentheseG(radiusbtn);
    boutonParentheseG.setFillColor(sf::Color(33, 33, 33));
    boutonParentheseG.setPosition(colonne4, ligne5);
    sf::Text textboutonParentheseG("(", font2, 30);
    textboutonParentheseG.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - width * 0.1 * 0.25 - 80 - 400);
    textboutonParentheseG.setFillColor(sf::Color::White);
    //Bouton asin
    sf::CircleShape boutonasin(radiusbtn);
    boutonasin.setFillColor(sf::Color(33, 33, 33));
    boutonasin.setPosition(colonne1, ligne6);
    sf::Text textboutonasin("asin", font2, 30);
    textboutonasin.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 - width * 0.02 * 0.125, height - width * 0.12 * 0.25 - 80 - 500);
    textboutonasin.setFillColor(sf::Color::White);

    //Bouton acos
    sf::CircleShape boutonacos(radiusbtn);
    boutonacos.setFillColor(sf::Color(33, 33, 33));
    boutonacos.setPosition(colonne2, ligne6);
    sf::Text textboutonacos("acos", font2, 30);
    textboutonacos.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1 * width * 0.02 * 0.125 + width * 0.25 * 0.25 * 0.9, height - width * 0.12 * 0.25 - 80 - 500);
    textboutonacos.setFillColor(sf::Color::White);

    //Bouton atan
    sf::CircleShape boutonatan(radiusbtn);
    boutonatan.setFillColor(sf::Color(33, 33, 33));
    boutonatan.setPosition(colonne3, ligne6);
    sf::Text textboutonatan("atan", font2, 30);
    textboutonatan.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height -width * 0.12 * 0.25 - 80 - 500);
    textboutonatan.setFillColor(sf::Color::White);
    //Bouton abs
    sf::CircleShape boutonParentheseD(radiusbtn);
    boutonParentheseD.setFillColor(sf::Color(33, 33, 33));
    boutonParentheseD.setPosition(colonne4, ligne6);
    sf::Text textboutonParentheseD(")", font2, 30);
    textboutonParentheseD.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - width * 0.12 * 0.25 - 80 - 500);
    textboutonParentheseD.setFillColor(sf::Color::White);
    //Bouton exp
    sf::CircleShape boutonexp(radiusbtn);
    boutonexp.setFillColor(sf::Color(33, 33, 33));
    boutonexp.setPosition(colonne1, ligne7);
    sf::Text textboutonexp("exp", font2, 30);
    textboutonexp.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 - width * 0.02 * 0.125, height - width * 0.14 * 0.25 - 80 - 600);
    textboutonexp.setFillColor(sf::Color::White);
    //Bouton ln
    sf::CircleShape boutonln(radiusbtn);
    boutonln.setFillColor(sf::Color(33, 33, 33));
    boutonln.setPosition(colonne2, ligne7);
    sf::Text textboutonln("ln", font2, 30);
    textboutonln.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1 * width * 0.02 * 0.125 + width * 0.25 * 0.25 * 0.9, height - width * 0.14 * 0.25 - 80 - 600);
    textboutonln.setFillColor(sf::Color::White);
    //Bouton sqrt
    sf::CircleShape boutonsqrt(radiusbtn);
    boutonsqrt.setFillColor(sf::Color(33, 33, 33));
    boutonsqrt.setPosition(colonne3, ligne7);
    sf::Text textboutonsqrt("sqrt", font2, 30);
    textboutonsqrt.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height - width * 0.14 * 0.25 - 80 - 600);
    textboutonsqrt.setFillColor(sf::Color::White);
    //Bouton pow
    sf::CircleShape boutonpow(radiusbtn);
    boutonpow.setFillColor(sf::Color(33, 33, 33));
    boutonpow.setPosition(colonne4, ligne7);
    sf::Text textboutonpow("x�", font2, 30);
    textboutonpow.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - width * 0.14 * 0.25 - 80 - 600);
    textboutonpow.setFillColor(sf::Color::White);

    //Test fonction
    vector <float> fonctionval;
    float nbpoints = 2500; //Nombre de points pour le tracage
    int borneinf = -tailledem / 2; //Borne infrieur du graphe
    int bornesup = tailledem / 2; //Borne suprieure du graphe

    for (float x = borneinf; x < bornesup; x = x + (tailledem / nbpoints)) { //On stocke dans levector les resultats

        fonctionval.push_back(cos(2*3.14*x)/x); // LA FONCTION Y=X dans le pushback
    }

    //Boucle d'�v�nements
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type) {

            case sf::Event::Closed:
                window.close();

            case sf::Event::MouseWheelMoved:

                if (event.mouseWheel.x > width * 0.25 && event.mouseWheel.y > height * 0.25) {

                    //Zoom
                    if (event.mouseWheel.delta > 0) {
                        view.zoom(0.9f);
                    }
                    //D�zoom
                    else if (event.mouseWheel.delta < 0) {
                        view.zoom(1.1f);

                    }
                }

                break;
            }

            //Saisie de texte
            if (event.type == sf::Event::TextEntered) {

                if (event.text.unicode < 128) {

                    s.push_back((char)event.text.unicode);
                    std::cout << s << std::endl;
                }

            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace) && s.size() != 0) {
                s.pop_back();
                std::cout << s << std::endl;
            }

            ///////////Gestion du clic bouton sur la calculatrice////////////////
            if (isSpriteHover(flechespriteg.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(-100.f, 0);
                }
            }

            if (isSpriteHover(flechesprited.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(100.f, 0);
                }
            }

            if (isSpriteHover(flechespriteh.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(0, 100);
                }
            }

            if (isSpriteHover(flechespriteb.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(0, -100);
                }
            }

            if (isSpriteHover(bouton0.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton0.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('0');
                    bouton0.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton1.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton1.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('1');
                    bouton1.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton2.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton2.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('2');
                    bouton2.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton3.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton3.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('3');
                    bouton3.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton4.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton4.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('4');
                    bouton4.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton5.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton5.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('5');
                    bouton5.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton6.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton6.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('6');
                    bouton6.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton7.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton7.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('7');
                    bouton7.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton8.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton8.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('8');
                    bouton8.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(bouton9.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                bouton9.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('9');
                    bouton9.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(boutonpoint.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonpoint.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('.');
                    boutonpoint.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(boutonvirgule.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonvirgule.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back(',');
                    boutonvirgule.setFillColor(sf::Color(51, 51, 51));
                }
            }

            if (isSpriteHover(boutonmoins.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonmoins.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('-');
                    boutonmoins.setFillColor(sf::Color(230, 134, 0));
                }
            }

            if (isSpriteHover(boutonplus.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonplus.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('+');
                    boutonplus.setFillColor(sf::Color(230, 134, 0));
                }
            }

            if (isSpriteHover(boutonfois.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonfois.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('*');
                    boutonfois.setFillColor(sf::Color(230, 134, 0));
                }
            }

            if (isSpriteHover(boutondivise.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutondivise.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('/');
                    boutondivise.setFillColor(sf::Color(230, 134, 0));
                }
            }
            if (isSpriteHover(boutonsin.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonsin.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('s');
                    boutonsin.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutoncos.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutoncos.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('c');
                    boutoncos.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutontan.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutontan.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('t');
                    boutontan.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonParentheseG.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonParentheseG.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back(')');
                    boutonParentheseG.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonasin.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonasin.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('s');
                    boutonasin.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonacos.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonacos.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('c');
                    boutonacos.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonatan.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonatan.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('t');
                    boutonatan.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonParentheseD.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonParentheseD.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back(')');
                    boutonParentheseD.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonexp.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonexp.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('e');
                    boutonexp.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonln.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonln.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('l');
                    boutonln.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonsqrt.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonsqrt.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('r');
                    boutonsqrt.setFillColor(sf::Color(33, 33, 33));
                }
            }
            if (isSpriteHover(boutonpow.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true) {

                boutonpow.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('p');
                    boutonpow.setFillColor(sf::Color(33, 33, 33));
                }
            }
        }

        window.clear(sf::Color(255, 255, 255));
        window.setView(view);
        int ChiffreGrille = 0;
        char bufferX[4];
        char bufferY[4];

        /////////////////////////Affichage graphique (en bas � droite)/////////////////////////////////////////

        //Cr�ation de la grille
        for (int dessinergrille = 0; dessinergrille < tailledem + 1; dessinergrille++) {

            //Cadrillage horizontal
            int tmp = taillecase * dessinergrille;
            int tmp2 = -tailledem / 2 + dessinergrille;
            sf::RectangleShape lineh(sf::Vector2f((float)(tailledem * taillecase) * 2, 1));
            lineh.setPosition((float)(-tailledem * taillecase), (-tailledem * taillecase) + (dessinergrille * taillecase + tmp));
            lineh.setFillColor(sf::Color(180, 180, 180));
            window.draw(lineh);

            //Cadrillage vertical
            sf::RectangleShape linev(sf::Vector2f(1, (tailledem * taillecase) * 2));
            linev.setPosition((float)(-tailledem * taillecase) + (dessinergrille * taillecase + tmp), (-tailledem * taillecase));
            linev.setFillColor(sf::Color(180, 180, 180));
            window.draw(linev);

            //Ecriture des labels sur le graphe
            _itoa_s(tmp2, bufferX, 10);
            _itoa_s(-tmp2, bufferY, 10);
            sf::Text textH(bufferX, font2, 12);
            textH.setPosition(sf::Vector2f(-tailledem * taillecase + taillecase * dessinergrille * 2 - 10, 0));
            textH.setFillColor(sf::Color(30, 30, 30));
            window.draw(textH);
            sf::Text textV(bufferY, font2, 12);
            textV.setPosition(sf::Vector2f(0, -tailledem * taillecase + taillecase * dessinergrille * 2));
            textV.setFillColor(sf::Color(30, 30, 30));
            window.draw(textV);
        }

        //Affichage fonction
        float ratio = tailledem / nbpoints;

        for (unsigned int j = 0; j < fonctionval.size() - 1; j++) {

            float distance = sqrt(pow((borneinf + (j + 1) * (tailledem / nbpoints)) - (borneinf + j * (tailledem / nbpoints)), 2) + pow((fonctionval[j + 1] - fonctionval[j]), 2));// DISTANCE ENTRE A ET B
            sf::RectangleShape trace(sf::Vector2f(distance * taillecase+1, 4));
            trace.setPosition((float)((-2 * bornesup * taillecase + 2 * j * ratio * taillecase)), -fonctionval[j] * taillecase * 2);
            trace.setFillColor(sf::Color::Red);
            window.draw(trace);
        }


        //Tracage de l'axe x
        sf::RectangleShape axeX(sf::Vector2f(100000, 3)); //AXE X
        axeX.setPosition((float)(-50000), 0);
        axeX.setFillColor(sf::Color(0, 0, 0));
        window.draw(axeX);
        /*//Affichage fl�che rep�re x
        sf::CircleShape triangleAxeX(10, 3);
        triangleAxeX.rotate(90);
        triangleAxeX.setFillColor(sf::Color(0, 0, 0));
        triangleAxeX.setPosition(width * 0.995, height * 0.625 - 8.5);
        window.draw(triangleAxeX);*/

        //Tracage de l'axe y
        sf::RectangleShape axeY(sf::Vector2f(3, 100000)); //AXE Y
        axeY.setPosition(0, -50000);
        axeY.setFillColor(sf::Color(0, 0, 0));
        window.draw(axeY);
        /*//Affichage fl�che rep�re y
        sf::CircleShape triangleAxeY(10, 3);
        triangleAxeY.setFillColor(sf::Color(0, 0, 0));
        triangleAxeY.setPosition(width * 0.625 - 9, height * 0.26);
        window.draw(triangleAxeY);*/

        //////////////Affichage zone calculette (� gauche)///////////////////////
        window.setView(window.getDefaultView());
        sf::RectangleShape rectanglecalculette(sf::Vector2f(width / 4, height));
        rectanglecalculette.setPosition(0, 0);
        rectanglecalculette.setFillColor(sf::Color::Black);
        window.draw(rectanglecalculette);
        //Ligne de s�paration entre la zone calculette et le graphique
        sf::RectangleShape ligneVerticale(sf::Vector2f(3, 3 * (width / 4)));
        ligneVerticale.setPosition(width / 4, height / 4);
        ligneVerticale.setFillColor(sf::Color(0, 0, 0));
        window.draw(ligneVerticale);

        //////////////Affichage zone console (en haut)///////////////////////
        sf::RectangleShape rectangleconsole(sf::Vector2f(width, height / 4));
        rectangleconsole.setPosition(0, 0);
        rectangleconsole.setFillColor(sf::Color(177, 187, 202));
        window.draw(rectangleconsole);
        //Ligne de s�paration entre la zone console et le graphique
        sf::RectangleShape ligneHorizontale(sf::Vector2f(width, 3));
        ligneHorizontale.setPosition(0, height / 4);
        ligneHorizontale.setFillColor(sf::Color(0, 0, 0));
        window.draw(ligneHorizontale);

        //////////////////Affichages/////////////////////////////////////////

        //Texte fonction 1
        sf::Text fonction1("Fonction 1:", font2, 40);
        fonction1.setPosition(sf::Vector2f(2, 5));
        fonction1.setFillColor(sf::Color(0, 0, 0));
        window.draw(fonction1);

        //Affichage fond �quation
        sf::RectangleShape zoneEquation(sf::Vector2f(width - 10, 50));
        zoneEquation.setFillColor(sf::Color(255, 255, 255));
        zoneEquation.setPosition(sf::Vector2f(5, 50));
        window.draw(zoneEquation);

        //Affichage Equation
        sf::Text text(s, font, 50);
        text.setPosition(sf::Vector2f(5, 45));
        text.setFillColor(sf::Color(0, 0, 0));
        window.draw(text);

        //Affichages fl�ches
        flechesprited.setScale(0.5, 0.5);
        flechesprited.setPosition(width * 0.30, height * 0.3);
        window.draw(flechesprited);
        flechespriteg.setScale(0.5, 0.5);
        flechespriteg.setPosition(width * 0.28, height * 0.33);
        window.draw(flechespriteg);
        flechespriteh.setScale(0.5, 0.5);
        flechespriteh.setPosition(width * 0.3, height * 0.33);
        window.draw(flechespriteh);
        flechespriteb.setScale(0.5, 0.5);
        flechespriteb.setPosition(width * 0.285, height * 0.3);
        window.draw(flechespriteb);

        //Affichage des boutons
        window.draw(bouton0);
        window.draw(textbouton0);
        window.draw(bouton1);
        window.draw(textbouton1);
        window.draw(bouton2);
        window.draw(textbouton2);
        window.draw(bouton3);
        window.draw(textbouton3);
        window.draw(bouton4);
        window.draw(textbouton4);
        window.draw(bouton5);
        window.draw(textbouton5);
        window.draw(bouton6);
        window.draw(textbouton6);
        window.draw(bouton7);
        window.draw(textbouton7);
        window.draw(bouton8);
        window.draw(textbouton8);
        window.draw(bouton9);
        window.draw(textbouton9);
        window.draw(boutonpoint);
        window.draw(textboutonpoint);
        window.draw(boutonvirgule);
        window.draw(textboutonvirgule);
        window.draw(boutonmoins);
        window.draw(textboutonmoins);
        window.draw(boutonplus);
        window.draw(textboutonplus);
        window.draw(boutonfois);
        window.draw(textboutonfois);
        window.draw(boutondivise);
        window.draw(textboutondivise);
        window.draw(boutonsin);
        window.draw(textboutonsin);
        window.draw(boutoncos);
        window.draw(textboutoncos);
        window.draw(boutontan);
        window.draw(textboutontan);
        window.draw(boutonParentheseG);
        window.draw(textboutonParentheseG);
        window.draw(boutonasin);
        window.draw(textboutonasin);
        window.draw(boutonacos);
        window.draw(textboutonacos);
        window.draw(boutonatan);
        window.draw(textboutonatan);
        window.draw(boutonParentheseD);
        window.draw(textboutonParentheseD);
        window.draw(boutonexp);
        window.draw(textboutonexp);
        window.draw(boutonln);
        window.draw(textboutonln);
        window.draw(boutonsqrt);
        window.draw(textboutonsqrt);
        window.draw(boutonpow);
        window.draw(textboutonpow);
        window.display();
    }

    return 0;
}